"""
1D example of the finite element method using FEniCS.
"""

from dolfin import *
import matplotlib.pyplot as pl

n = 45 # number of elements
mesh = UnitIntervalMesh(n)

V = FunctionSpace(mesh, 'CG', 1) # FEM space

# trial and test functions
u = TrialFunction(V)
v = TestFunction(V)

# bilinear form and linear functional
class Material(UserExpression):

    def eval(self, value, x):
        if x<0.5:
            value[0] = 1
        else:
            value[0] = 2

    def value_shape(self):
        return ()


material = Material(degree=1)

a = inner(material*grad(u), grad(v))*dx
f = Expression('0', degree=0)
L = f*v*dx

def left_boundary(x):
    return x[0] < DOLFIN_EPS

def right_boundary(x):
    return x[0] > 1-DOLFIN_EPS

Lbc = DirichletBC(V, Constant(0.0), left_boundary)
Rbc = DirichletBC(V, Constant(20.0), right_boundary)


# Compute solution
u = Function(V)
solve(a == L, u, [Lbc, Rbc])
plot(u, title='solution u')
pl.show()
plot(u.dx(0), title='derivative of solution u')
pl.show()
print('END')
