"""
1D example of the finite element method using FEniCS.
"""

from dolfin import *
import matplotlib.pyplot as pl


n = 45 # number of elements
mesh = UnitIntervalMesh(n)

V = FunctionSpace(mesh, 'CG', 1) # FEM space

# example of basis functions
basis = Function(V)
basis.vector()[5] = 1
plot(basis, title='example of basis function')
pl.show()

# trial and test functions
u = TrialFunction(V)
v = TestFunction(V)

# bilinear form and linear functional
a = inner(grad(u), grad(v))*dx
f = Expression('5', degree=1)
L = f*v*dx + 1*v*ds

# Define Dirichlet boundary (x = 0)
def left_boundary(x):
    return x[0] < DOLFIN_EPS

bc = DirichletBC(V, Constant(0.0), left_boundary)

# Compute solution
u = Function(V)
solve(a == L, u, bc)
plot(u, title='solution u')
pl.show()
plot(u.dx(0), title='derivative of solution u')
pl.show()
print('END')
