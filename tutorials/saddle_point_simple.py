"""
This simple example shows the minimisation of a two-dimensional function
with an affine constraint, see lecture notes (saddle point problems) for an explanation.

minimise J(x) over all x

J(x) = 0.5*(x1^2+x2^2 - x1*x2-2x1-x2)

subject to an affine constraint
Bx=g
"""

import numpy as np
import matplotlib.pyplot as pl

# definition of problem without constraint
A = np.array([[2, -1], [-1, 2]])
b = np.array([1., 0.5])

u = np.linalg.solve(A, b)
print('unconstraint solution u=', u)
print('norm of residum', np.linalg.norm(b-A.dot(u)))

# definition of problem with constraint
B = np.atleast_2d(np.array([3, 1], dtype=np.float))
g = np.array([1])
bfun = lambda t: -B[0,0]/B[0,1]*t + g[0]/B[0,1]

# setting a bigger matrix
C = np.bmat([[A, B.T], [B, np.zeros([1,1])]])
bg = np.hstack([b, g])
print(C)
print(bg)
ulam = np.linalg.solve(C, bg)
ucon = ulam[:2] # constraint solution u
print('constraint x=', ucon)

# eigenvalues of C shows that there are negative eigenvalues corresponding to constraint
eigs, vecs = np.linalg.eigh(C)
print('eigenvalues', eigs)
print('eigenvectors\n', vecs)

print(vecs.dot(np.diag(eigs)).dot(vecs.T))
P = B.T.dot(np.linalg.inv(B.dot(B.T))).dot(B)

print(P)
print(P.shape)
print('end')

#### plotting ############
fun = lambda x, y: 0.5*(A[0,0]*x**2 + 2*A[0,1]*x*y + A[1,1]*y**2) - b[0]*x - b[1]*y
npl = 1e3
X, Y = np.meshgrid(np.linspace(-.5, 1.5, npl), np.linspace(-.5, 1.5, npl), sparse=False)
fig = pl.figure(figsize=(6,6))
ptype = 'contour'
filen = None
pl.axis('equal')
CS = pl.contour(X, Y, fun(X, Y), np.linspace(-1.0, 2.0, 30))
tpl = np.linspace(-1, 1, 1e2)
pl.plot(tpl, bfun(tpl), 'g-', label='constraint space')
pl.plot(u[0], u[1], 'ko', label='unconstraint solution')
pl.plot(ucon[0], ucon[1], 'rs', label='constraint solution')
dx = b/np.linalg.norm(b)
# pl.arrow(ucon[0], ucon[1], dx[0], dx[1])

pl.xlabel(r'$x_1$')
pl.ylabel(r'$x_2$')
pl.axis('equal')
pl.xlim([-0.5, 1.5])
pl.ylim([-0.5, 1.5])
pl.legend(loc='best')
pl.clabel(CS, inline=1, fontsize=10)

if filen is None:
    pl.show()
else:
    pl.savefig(filen)
print('END')
