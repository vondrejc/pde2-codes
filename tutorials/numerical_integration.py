"""
This script visualises numerical integration points
on a reference triangle and a square.
"""

from ffc.fiatinterface import create_quadrature

cells=['triangle','quadrilateral']
degree=4 # the degree of polynomial that is integrated exactly

for cell in cells:

    ip, iw=create_quadrature(cell, degree=degree)
    print("= {} ========".format(cell))
    print("points={}".format(ip))
    print("weights={}".format(iw))
    import matplotlib.pyplot as pl
    pl.figure()
    pl.title(cell)
    for ip0 in ip:
        pl.plot(ip0[0], ip0[1], 'x')
    pl.xlim([0,1])
    pl.ylim([0,1])
    pl.grid()
    pl.xlabel('Coordinate $x_1$')
    pl.ylabel('Coordinate $x_2$')
pl.show()