"""
This script assembles the system matrix and
solves that system for a general nonhomogeneous Dirichlet boundary conditions.
"""

from dolfin import *
import numpy as np


n = 5 # number of elements
dim=1
if dim==1:
    mesh = UnitIntervalMesh(n)

V = FunctionSpace(mesh, 'CG', 1) # FEM space

# trial and test functions
u = TrialFunction(V)
v = TestFunction(V)

# bilinear form and linear functional
a = u*v*dx
f = Expression('x[0]*(1-x[0])', degree=2)
L = f*v*dx

# Define Dirichlet boundary (x = 0)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0]>1-DOLFIN_EPS

bc = DirichletBC(V, Constant(0.0), boundary)

# Compute solution
u = Function(V)
solve(a == L, u, bc)
b=assemble(L)

# assembling a system matrix
A = EigenMatrix()
A = assemble(a, tensor=A)

bcinds = list(bc.get_boundary_values().keys()) # indices of boundary
print(bcinds)
uD=np.array(list(bc.get_boundary_values().values())) # the values on boundary
print(uD)

adof = np.setdiff1d(np.arange(V.dim(), dtype=np.int32),
                              bcinds).astype(np.int32) # active DOFs

# submatrices corresponding to active DOFs and to BC
A_PP = A.array()[adof, :][:, adof]
A_PD = A.array()[adof, :][:, bcinds]
A_DD = A.array()[bcinds, :][:, bcinds]
print(A.array())
print(A_PP)

# finding the solution
u2 = np.zeros(V.dim()) # preallocationg
u2[bcinds] = uD # setting the values on Dirichlet boundary
u2[adof] = np.linalg.solve(A_PP, b[adof]-A_PD.dot(uD)) # solving the system for active DOFs

# checking the solution
print('norm(u-u2) =',np.linalg.norm(u2-u.vector()))
print('END')
