"""
This script visualise a linear Lagrange basis function.
"""

import numpy as np
from dolfin import *
import matplotlib.pyplot as pl

n=5
mesh=UnitSquareMesh(n,n)
V=FunctionSpace(mesh, 'CG', 1)
f=Function(V)
f.vector()[17]=1
print(f.vector().get_local())
plot(f)
pl.show()

print('END')
