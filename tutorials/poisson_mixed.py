"""
The primal formulation of Poisson's equation:

         -div(grad(u)) = f    in Omega
              du/dn = g    on Gamma_N
                  u = u_D  on Gamma_D

The corresponding weak (variational problem)

    <grad(u), grad(v)> = <f, v> + <g, v>  for all v

"""


from dolfin import *
import numpy as np
import matplotlib.pylab as pl

n=10 # number of elements
p=1 # polynomial order

mesh=UnitSquareMesh(n, n)
# Define source functions

f=Expression("1000*(x[0] - 1)*x[0]*(x[1] - 1)*x[1]", degree=4) # source of heat
uD=Expression("1", degree=0) # Dirichlet values
sigN=Expression("10", degree=0) # Neumann values

# Define Boundaries
class Dirichlet_boundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (x[0]<DOLFIN_EPS or x[0]>1.0-DOLFIN_EPS)

dirichlet_boundary=Dirichlet_boundary()

class Neumann_boundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (x[1]<DOLFIN_EPS or x[1]>1.0-DOLFIN_EPS)

neumann_boundary=Neumann_boundary()

boundary_markers=MeshFunction('size_t', mesh, mesh.geometry().dim()-1)
boundary_markers.set_all(0)
neumann_boundary.mark(boundary_markers, 1)
dirichlet_boundary.mark(boundary_markers, 2)
ds=Measure('ds', domain=mesh, subdomain_data=boundary_markers)

def solve_primal(p=1):
    # Define function spaces and mixed (product) space
    V=FunctionSpace(mesh, "CG", p)

    # Define trial and test functions
    u=TrialFunction(V)
    v=TestFunction(V)

    # Define variational form
    a=dot(grad(u), grad(v))*dx
    L=f*v*dx+sigN*v*ds(1)

    bc=DirichletBC(V, uD, dirichlet_boundary)
    # Compute solution
    u=Function(V)
    solve(a==L, u, bc)
    return u

def solve_mixed_primal(p=1):
    Psig=VectorElement("DG", mesh.ufl_cell(), p-1)
    Pu=FiniteElement("CG", mesh.ufl_cell(), p)
    W=FunctionSpace(mesh, Psig*Pu)

    # Define trial and test functions
    (sigma, u)=TrialFunctions(W)
    (tau, v)=TestFunctions(W)
    # Define variational form
    a=(dot(sigma, tau)-dot(grad(u), tau)-dot(sigma, grad(v)))*dx
    L=-f*v*dx-sigN*v*ds(1)

    bc=DirichletBC(W.sub(1), uD, dirichlet_boundary)
    # Compute solution
    w=Function(W)
    solve(a==L, w, bc)
    (sigma, u)=w.split(deepcopy=True)
    return (sigma, u)

def solve_mixed_dual(p=1):
    Psig=FiniteElement("RT", mesh.ufl_cell(), p)
    Pu=FiniteElement("DG", mesh.ufl_cell(), p-1)
    W=FunctionSpace(mesh, Psig*Pu)

    # Define trial and test functions
    (sigma, u)=TrialFunctions(W)
    (tau, v)=TestFunctions(W)

    n=FacetNormal(mesh)
    a=(dot(sigma, tau)+div(tau)*u+div(sigma)*v)*dx
    L=-f*v*dx + uD*dot(tau,n)*ds(2)

    # Define function G such that G \cdot n = g
    class BoundarySource(UserExpression):
        def __init__(self, mesh, **kwargs):
            self.mesh=mesh
            super().__init__(**kwargs)

        def eval_cell(self, values, x, ufc_cell):
            cell=Cell(self.mesh, ufc_cell.index)
            n=cell.normal(ufc_cell.local_facet)
            g = sigN(x)
            values[0]=g*n[0]
            values[1]=g*n[1]

        def value_shape(self):
            return (2,)

    G=BoundarySource(mesh, degree=1)

    bc=DirichletBC(W.sub(0), G, neumann_boundary)

    # Compute solution
    w=Function(W)
    solve(a==L, w, bc)
    (sigma, u)=w.split(deepcopy=True)
    return (sigma, u)

uP=solve_primal(p=p)
(sigmaMP, uMP)=solve_mixed_primal(p=p)
(sigmaMD, uMD)=solve_mixed_dual(p=p)

normdif = np.linalg.norm(uP.vector().get_local()-uMP.vector().get_local())
print('norm of difference between primal and primal-mixed form. = {}'.format(normdif))

# energies
n=FacetNormal(mesh)
Jprim = lambda u: assemble(0.5*inner(grad(u), grad(u))*dx-f*u*dx-sigN*u*ds(1))
Jdual = lambda sigma: assemble(-0.5*inner(sigma, sigma)*dx+uD*dot(sigma,n)*ds(2))
print(Jprim(uP))
print(Jprim(uMP))
print(Jdual(sigmaMD))

print('END')
