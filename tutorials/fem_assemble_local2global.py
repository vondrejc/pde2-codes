"""
This script shows how the system matrix is assembled from the local system matrices.
"""

from __future__ import print_function
from dolfin import *
import numpy as np
import matplotlib.pyplot as pl


dim=2 # dimension of the problem
N=5 # number of elements
pol_order=1 # polynomial order

if dim==1:
    mesh=UnitIntervalMesh(N)
elif dim==2:
    mesh=UnitSquareMesh(N,N)
elif dim==3:
    mesh=UnitCubeMesh(N,N)

V=FunctionSpace(mesh,'CG',pol_order)
basis = Function(V)
basis.vector()[12] = 1
plot(basis, title='example of basis function')
pl.show()
# A = Expression('x[0]*x[1]', degree=2)
A = Expression('1', degree=0)

u=TrialFunction(V)
v=TestFunction(V)
Aform=inner(grad(u),grad(v))*dx

Amat = assemble(Aform).array()
print('shape of global system matrix', Amat.shape)

## local assembling
Amat2 = np.zeros([V.dim(), V.dim()])
for cell in cells(mesh):
    print('-- element no. = {}'.format(cell.index()))
    Aloc = assemble_local(Aform, cell)
    local_to_global_map = V.dofmap().cell_dofs(cell.index())
    Amat2[np.ix_(local_to_global_map,local_to_global_map)] += Aloc
    print('cell vertices:\n', np.array(cell.get_vertex_coordinates()).reshape(-1,2))
    print('coordinates of DOFs:\n',V.element().tabulate_dof_coordinates(cell))
    print('local system matrix:\n',Aloc)
    print('indices in the global system matrix:',local_to_global_map)

print('=======================================')
print('norm of difference between matrices = {}'.format(np.linalg.norm(Amat-Amat2)))
print(Amat)
print('end')
