"""
This examples implements the Stokes problem,
i.e. the stationary irrotational fluid flow.
"""


from dolfin import *

# Load mesh
mesh = UnitSquareMesh(10,10)

# Build function space
P2 = VectorElement("Lagrange", mesh.ufl_cell(), 2)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)

# Next, we define the boundary conditions.

# Boundaries
def right(x, on_boundary): return x[0] > (1.0 - DOLFIN_EPS)
def top(x, on_boundary): return x[1] > 1.0 - DOLFIN_EPS
def left_bottom(x, on_boundary):
    return x[0] < DOLFIN_EPS or x[1] < DOLFIN_EPS

# No-slip boundary condition for velocity
noslip = Constant((0.0, 0.0))
bc0 = DirichletBC(W.sub(0), noslip, left_bottom)

# Inflow boundary condition for velocity
inflow = Expression(("-sin(x[1]*pi)", "0.0"), degree=2)
bc1 = DirichletBC(W.sub(0), inflow, right)

# Collect boundary conditions
bcs = [bc0, bc1]

# Define variational problem
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

U=Function(W)
solve(a==L, U, bcs)

# Get sub-functions
u, p = U.split() # to velocity and pressure


import matplotlib.pyplot as plt
plot(u, title='Velocity')
plt.savefig('figures/Stokes_velocity.pdf')
plot(p, title='Pressure')
plt.savefig('figures/Stokes_pressure.pdf')

print('END')
