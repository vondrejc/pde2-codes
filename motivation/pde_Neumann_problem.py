from __future__ import division, print_function
import numpy as np
from fenics import *
from scipy.integrate import odeint
from scipy.sparse.linalg import eigsh, svds

n = 5

print("""This script focuses on the Neumann problem, which is a scalar elliptic PDE
with purely Neumann boundary conditions. Since there are no essential (Dirichlet)
boundary conditions and only gradient of the solution occurs in the formulation,
there are many solutions of the Neumann problem that differs by a constant.
Therefore the function space has to be contrainet, e.g. considering functions with zero mean.
In the system matrix (without reduction due to essential boundary conditions), it is reflected
by the fact that there is one zero eigenvalue and a corresponding eigenvector is of constant values,
i.e. representing constant functions. The matrix is singular and there are few remedies.
One can fix one point of the system (Dirichlet boundary condition in one point), which reduces
the size of matrix by 1 and the matrix become singular. Another option is to use Lagrange
multipliers to enforce the functions to be zero-mean, which leads to a saddle point problem.""")

print('## generating model problem ##################################')
mesh = UnitSquareMesh(n,n) # FEM mesh
V = FunctionSpace(mesh, 'CG', 2) # FEM space
# boundary conditions

u = TrialFunction(V)
v = TestFunction(V)


A=assemble(inner(grad(u),grad(v))*dx)
Am=A.array()

lams, vecs = np.linalg.eigh(Am)

print('smallest eigenvalue=',lams[0])
print('corresponding eigenvector=',vecs[:,0])
print('END')
