from __future__ import division, print_function
import numpy as np
from fenics import *
from scipy.integrate import odeint
from scipy.sparse.linalg import eigsh, svds

n = 51 # discretisation parameter: number of elements in each direction
n = 21
tt = np.linspace(0, 50, 1e1) # time steps
tt = np.linspace(0, 500, 1e2) # time steps
animation_flag=1 # plotting (1) or suppressing to plot (0)

print('## generating model problem ##################################')
mesh = UnitSquareMesh(n,n) # FEM mesh
V = FunctionSpace(mesh, 'CG', 1) # FEM space
# boundary conditions
u0 = interpolate(Expression('0', degree=1), V)
f = Expression('100',degree=1)
bc = DirichletBC(V, Constant(0.), lambda x, on_boundary: on_boundary)

u = TrialFunction(V)
v = TestFunction(V)

print('## FULL COMPUTATION ##########################################')
uh=Function(V)
solve(inner(grad(u),grad(v))*dx == f*v*dx, uh, bc)


print('## PLOTTING and ANIMATIONS ###################################')
from mpl_toolkits.mplot3d import axes3d
import matplotlib as mpl
import matplotlib.pyplot as pl
# import matplotlib.animation as animation
import os
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams.update({'text.usetex' : True,
            'text.latex.unicode': True,
            'font.size': 20,
            'legend.fontsize': 14,
              })

for directory in ['figures', 'videos']:
    if not os.path.exists(directory):
        os.makedirs(directory)

cm=plot(uh)
pl.colorbar(cm)
pl.savefig('figures/pde_elliptic.pdf')
print('END')
