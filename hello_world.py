print('Hello world!')
import sys
print('python version (recommended 3<): ' + sys.version)

import numpy as np
import scipy as sp
import sympy as syp
import matplotlib as pl

print('\nRECOMMENDED VERSIONS...')
print('numpy version (1.16<): ' + np.__version__)
print('scipy version (1.2<): ' + sp.__version__)
print('sympy version (1.1<): ' + syp.__version__)
print('matplotlib version  (3<): ' + pl.__version__)

import dolfin
print('\nFEniCS version and available solvers...')
print('fenics/dolfin version  (2018<): {}'.format(dolfin.__version__))
print('feniss linear_algebra_backend: '+ dolfin.parameters['linear_algebra_backend'])

print(dolfin.list_linear_solver_methods())
print(dolfin.list_krylov_solver_methods())
print(dolfin.list_krylov_solver_preconditioners())

print("END")
