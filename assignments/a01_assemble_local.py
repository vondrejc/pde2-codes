from dolfin import *

import numpy as np
import sympy as sp
import matplotlib.pylab as plt

# nodes of a reference element
nodesR=np.array([[0., 0.],
                  [1., 0.],
                  [0., 1.]])

# nodes for a scaled element
ep=0.5
nodes1=np.array([[0., 0.],
                  [ep, 0.],
                  [0., ep]])

# nodes for a general element
nodes2=np.array([[2., 1.],
                  [3.5,-1.],
                  [5., 2.1]])

# choose here which nodes you would like to use
nodes=nodes2

cells=np.array([[0, 1, 2]], dtype=np.uintp) # connection of nodes (defines element)

print('-- Fenics assembling ---')
# this piece of code creates a mesh containing one element only
mesh=Mesh()
editor=MeshEditor()
editor.open(mesh, 'triangle', 2, 2)
editor.init_vertices(3)
editor.init_cells(1)

[editor.add_vertex(i, n) for i, n in enumerate(nodes)]
[editor.add_cell(i, n) for i, n in enumerate(cells)]
editor.close()

# plot(mesh)
# plt.show()

V=FunctionSpace(mesh, 'CG', 1)

u=TrialFunction(V)
v=TestFunction(V)

A=assemble(u*v*dx)
print(A.array())

B=assemble(inner(grad(u), grad(v))*dx)
print(B.array())

print('--  assembling ---')

# here you write your own code
mesh=Mesh()
editor=MeshEditor()
editor.open(mesh, 'triangle', 2, 2)
editor.init_vertices(3)
editor.init_cells(1)

[editor.add_vertex(i, n) for i, n in enumerate(nodes)]
[editor.add_cell(i, n) for i, n in enumerate(cells)]
editor.close()

# plot(mesh)
# plt.show()

V=FunctionSpace(mesh, 'CG', 1)

u=TrialFunction(V)
v=TestFunction(V)

A=assemble(u*v*dx)
print(A.array())

B=assemble(inner(grad(u), grad(v))*dx)
print(B.array())

print('--  assembling ---')

# here you write your own code
# # basis functions and their derivative
bas=[lambda x: x[0],
       lambda x: 1.-x[0]-x[1],
       lambda x: x[1]]

grad_bas=[lambda x: np.array([ 1., 0.]),
          lambda x: np.array([-1.,-1.]),
          lambda x: np.array([ 0., 1.])]


class Mapping():
    def __init__(self, nodes):
        self.nodes=nodes
        self.b=nodes[0]
        self.A=np.vstack([nodes[1]-nodes[0], nodes[2]-nodes[0]]).T
        self.Ai=np.linalg.inv(self.A)

    def __call__(self, hx):
        return self.A.dot(hx)+self.b

    def jacobian(self):
        return self.A

    def det_jacobian(self):
        return np.linalg.det(self.A)

# print 'nodes\n', nodes
F=Mapping(nodes)
print('A=\n', F.A)
print('det=|J|', F.det_jacobian())
print('Ai=\n', F.Ai)

print('A=', sp.latex(sp.Matrix(F.A)))
print('b=', sp.latex(sp.Matrix(F.b)))


class Integration():
    def __init__(self, order):
        if order==1:
            self.iw=np.array([1./2])
            self.ip=np.array([[1./3, 1./3]])
        elif order==2:
            self.iw=1./6*np.ones(3)
            self.ip=np.array([[1./6, 1./6],
                                [2./3, 1./6],
                                [1./6, 2./3]])
        else:
            raise NotImplementedError()

    def get_integration(self):
        return self.iw, self.ip

# # terms
A2=np.zeros([3, 3])
B2=np.zeros([3, 3])
iw, ip=Integration(2).get_integration()
dJ=F.det_jacobian()
Ai=F.Ai
for ii in range(3):
    for jj in range(3):
        for w, p in zip(iw, ip):
            A2[ii, jj]+=w*bas[ii](p)*bas[jj](p)*dJ
            inn=np.inner(grad_bas[ii](p).dot(Ai), grad_bas[jj](p).dot(Ai))
            B2[ii, jj]+=w*inn*dJ

print(A2)
print(B2)

print('--control---')
print(np.linalg.norm(B2-B.array()))
print(np.linalg.norm(A2-A.array()))
print('END')
