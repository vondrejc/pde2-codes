# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 20:38:03 2019

@author: mohit
"""
import numpy as np
import matplotlib.pyplot as pl
import scipy.interpolate as interpolate

f = lambda x: np.exp(x) # Function
df = lambda x: np.exp(x) # Derivation of a function
bound = [-1, 1] # Interval
x = np.linspace(bound[0], bound[1], 1e3) # sample points to plot original function
int_exact = df(bound[1])-df(bound[0])

# Function to get integration points and weights
def newton_cotes_rules(node, bound):
    pi = np.linspace(bound[0], bound[1], node)
    wi = np.zeros_like(pi)
    for ii in range(node):
        vals = np.zeros(node)
        vals[ii] = 1.
        lagpol = interpolate.lagrange(pi, vals)
        lagpol_int = lagpol.integ(1)
        wi[ii] = lagpol_int(bound[1])-lagpol_int(bound[0])
    return pi, wi

max_nodes = 30 # Number of nodes
nodes = np.arange(1,max_nodes)
error_nc = np.zeros(nodes.size)
error_gl = np.zeros(nodes.size)

# Numerical integration for different number of nodes
for ii, node in enumerate(nodes):
    pi, wi = newton_cotes_rules(node, bound)
    int_nc = np.sum(wi*f(pi))
    error_nc[ii]=abs(int_exact - int_nc)

    piG, wiG = np.polynomial.legendre.leggauss(node)
    int_gl = np.sum(wiG*f(piG))
    error_gl[ii]=abs(int_exact - int_gl)

print("""Observe that error of the Gauss-Legendre integration stabilises
at the level of computer accuracy
while the error of Newton-Cotes integration is getting worse
for integration rule with more that approx. 11 points.""")
# Plot
pl.figure()
pl.title('Error of the numerical integration', fontsize=12)
pl.semilogy(nodes, error_nc, 'k-.', label='Newton-Cotes Integration')
pl.semilogy(nodes, error_gl, 'r--', label='Gauss-Legendre Integration')
pl.gca().set(xlabel='Number of Nodes', ylabel='Error')
pl.legend(loc='best')
pl.show()
