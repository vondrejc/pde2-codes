print("""
Assingment 5, Exercise 1
====================================
""")

from dolfin import *
import numpy as np
import sympy as sp

# nodes of a reference element
nodesR=np.array([[0., 0.],
                  [1., 0.],
                  [0., 1.]])

# nodes for a scaled element
ep=0.5
nodes1=np.array([[0., 0.],
                  [ep, 0.],
                  [0., ep]])

# nodes for a general element
nodes2=np.array([[2., 1.],
                  [3.5,-1.],
                  [5., 2.]])

# choose here which nodes you would like to use
print('Change the nodes to get different element (the element can be also plotted).')
nodes=nodes2

cells=np.array([[0, 1, 2]], dtype=np.uintp) # connection of nodes (defines element)

# this piece of code creates a mesh containing one element only
mesh=Mesh()
editor=MeshEditor()
editor.open(mesh, 'triangle', 2, 2)
editor.init_vertices(3)
editor.init_cells(1)

[editor.add_vertex(i, n) for i, n in enumerate(nodes)]
[editor.add_cell(i, n) for i, n in enumerate(cells)]
editor.close()

plot(mesh)

## finite element spaces for dual-mixed formulation ##################
RT=VectorElement("RT", mesh.ufl_cell(), 1)
DG=FiniteElement("DG", mesh.ufl_cell(), 0)
VRT = FunctionSpace(mesh, RT) # Raviart-Thomas elements
VDG = FunctionSpace(mesh, DG) # Discontinuous Galerkin space

VRT = FunctionSpace(mesh, 'RT', 1) # Raviart-Thomas elements
VDG = FunctionSpace(mesh, 'DG', 0) # Discontinuous Galerkin space

# trial and test functions
sig = TrialFunction(VRT)
tau = TestFunction(VRT)
v = TestFunction(VDG)

# generation of stiffness matrix
A = assemble(inner(sig,tau)*dx)
B = assemble(div(sig)*v*dx)

Am = A.array()
Bm = B.array()
print('Discretisation matrix of term inner(u, v)*dx =')
print(Am)
print('Discretisation matrix of term div(u)*v*dx =')
print(Bm)

print('Implement the assembling of the two matrices!')

# now implement the stiffness matrix Am without Fenics
from utils.RTN import (get_edge_length, get_normals, get_dofs_coor, get_basis_fun)

edge_length=get_edge_length(nodes) # Getting edge_length(Pi-Pj) to which Outer normal (nij) of a triangle T is orthogonal
normals=get_normals(nodes) # Getting Outer normal (nij) of a triangle T # nij.(Pi-Pj) = 0
dof_coor=get_dofs_coor(nodes) # Getting middle point (eij) of the edge (Eij) opposite to point (Pk)
bas_RTN=get_basis_fun(nodes) # Getting basis function (si)

# dirac delta property of Raviart-Thomas basis functions
print('\nDirac delta property of Raviart-Thomas basis functions...')
for ii in range(3):
    for jj in range(3):
        print((np.dot(bas_RTN[jj](dof_coor[ii]), normals[ii])*edge_length[ii])) # For basis functions

# dirac delta property of Raviart-Thomas function space
print('\nDirac delta property of Raviart-Thomas function space...')
for ii in range(3):
    w=Function(VRT)
    w.vector()[ii]=1
    print((np.dot(w(dof_coor[ii]), normals[ii])*edge_length[ii])) # For Raviart-Thomas space

# # mappings and integration points
from utils.RTN import Mapping, Integration

mapF=Mapping(nodes)
iw, ip=Integration(order=2).get_integration() # generate integration points
detJT=mapF.det_jacobian() # determinant of Jacobian

# assemble energetic term in true element
Aloc=np.zeros([3, 3])
for ii in range(3):
    for jj in range(3):
        for iq in range(3): # loop over quadrature points
            Aloc[ii, jj]+=iw[iq]*np.dot(bas_RTN[ii](mapF(ip[iq])),
                                        bas_RTN[jj](mapF(ip[iq])))*detJT
print('\nLocal stiffness matrix for term "inner(sig,tau)*dx"')
print(Aloc)

print('\nLocal stiffness matrix for term "div(sig)*v*dx"')
# now implement the stiffness matrix Bm without Fenics
bas_RTN_B=get_basis_fun(nodes,'div') # Getting basis function (si)
Bloc=np.zeros([3])
for ii in range(3):
    for iq in range(3): # loop over quadrature points
        Bloc[ii]+=iw[iq]*(bas_RTN_B[ii](mapF(ip[iq])))*detJT
print('\nLocal stiffness matrix for term "div(sig)*v*dx"')
print(Bloc)
print('END')
