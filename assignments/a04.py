"""
Assignment 4
"""

import scipy.io as io
import numpy as np
import scipy.sparse
import scipy.sparse.linalg
# from generate_ABfg import generate
import scipy.sparse.linalg as scslin
# generate(n=5)

# loading of data and setting the matrices
data=io.loadmat('ABfg')
Am=data['A']
Bm=data['B']
f=data['f'].squeeze()
g=data['g'].squeeze()
fg=np.hstack([f, g])
Cm=scipy.sparse.bmat([[Am, Bm.T],
                       [Bm, None]], format='csr')

print('Size of A matrix = ', Am.shape)
print('Size of B matrix = ', Bm.shape)
print('Size of C matrix = ', Cm.shape)

# # TASK A:
print('''\n ****************** Exercise 1 (a) ******************
To solve a linear saddle point system, one should choose a robust and
less memory demanding solver. Cholesky Decomposition has very less sparsity.
And LU decomposition and Conjugate gradients solve a system with least
computatiional effort. While methods like MNRES,GMRES,and Bi-CGSTAB can
be used when we deal with large sparse matrices, usually, it converges
slowly compared to other methods.''')

# solving the linear system by direct solver
xlam=scipy.sparse.linalg.spsolve(Cm, fg)
print ('Norm of residual using direct solver, ', np.linalg.norm(Cm.dot(xlam)-fg))
u=xlam[0:Am.shape[0]]
lamda=xlam[Am.shape[0]:(Am.shape[0]+Bm.shape[0])]

# Solving using LU decomposition
LU=scipy.sparse.linalg.splu(Cm.tocsc())
xlam2=LU.solve(fg)
print ('Norm of residual using LU decomposition, ', np.linalg.norm(xlam-xlam2))

# Solving using biconjugate gradient method
xlam_bicg, info=scipy.sparse.linalg.bicg(Cm, fg, x0=None, tol=1e-08, maxiter=1e4)
print ('Norm of residual using BiCG method, ', np.linalg.norm(xlam_bicg-xlam))

# Solving using gmres method
xlam_gmres, info=scipy.sparse.linalg.gmres(Cm, fg, x0=None, tol=1e-08, maxiter=1e4)
print ('Norm of residual uding GMRES method, ', np.linalg.norm(xlam_gmres-xlam2))

## TASK B: matrix values ####################################
print(''' \n ****************** Exercise 1 (b) ******************
Sadlle-point constants...''')
eigsA=np.linalg.eigvalsh(Am.todense())

# elipticity constant - alpha > 0 that means Eigenvalues of A are positive
alpha=eigsA.min()
print ('elipticity constant, alpha =', alpha)

# constant of continuity of bilinear form a
CA=eigsA.max()
CA2=np.linalg.norm(Am.todense(), 2) # another possibility to calculate CA
print ('constant of continuity of bilinear form a is ', CA)

# constant of continuity of bilinear form b
U, D, V=np.linalg.svd(Bm.todense())
CB=np.linalg.norm(Bm.todense(), 2)
CB2=D.max()
print ('constant of continuity of bilinear form b is ', CB)

# constant of inf-sup condition
beta=D.min()
print ('constant of inf-sup condition, beta =', beta)

# Here, both alpha and beta are greater than zero. That means,
# there exist unique solution for f and g.

## TASK C: Optimality condition checking for saddle point problems#########################
print ('''\n ****************** Exercise 1 (c) ******************
Optimality condition...''')
L=lambda v, mu: 0.5*np.dot(Am.dot(v), v)-np.dot(f, v)+np.dot(Bm.dot(v), mu)-np.dot(g, mu)
vrand=np.random.random(Am.shape[0])
murand=np.random.random(Bm.shape[0])

# Lagrangian at optimum u and random myu
L1=L(u, murand)
print('L(u, mu) = ', L1)
# Lagrangian at optimum u and optimum myu
L2=L(u, lamda)
print('L(u, lamda) = ', L2)
# Lagrangian at random u and optimum myu
L3=L(vrand, lamda)
print('L(v, lamda) = ', L3)

# Here, L(u,mu) <= L(u,lamda) <= L(v,lamda) -- Optimality condition holds for random v and mu.

## TASK D: solution by conjugate gradients (CG) on a subspace ############################
print ('''\n ****************** Exercise 1 (d) ******************
Solution by CG on a subspace...''')
invBB=scipy.sparse.linalg.splu(Bm.dot(Bm.T))
x0=Bm.T.dot(invBB.solve(g))
print ('norm(B*x0 - g) = ', np.linalg.norm(Bm.dot(x0)-g))

# projection matrix/operator

P=lambda x: Bm.T.dot(invBB.solve(Bm.dot(x)))
Q=lambda x: x-P(x)
QA=lambda x: Q(Am.dot(x))

# checking that the projection is a projection
xrand=np.random.random(Am.shape[0])
print ('norm(P(xrand)-P*P(xrand))', np.linalg.norm(P(xrand)-P(P(xrand))))

# checking that the projection is orthogonal projection (it has to be symmetric)
yrand=np.random.random(Am.shape[0])
print('norm(P(xrand).dot(yrand)-P(yrand).dot(xrand) = {}'.format(np.linalg.norm(P(xrand).dot(yrand)-P(yrand).dot(xrand))))

QAlinoper=scslin.LinearOperator(shape=Am.shape, matvec=QA, dtype=np.float)
xV, info=scipy.sparse.linalg.cg(QAlinoper, Q(f-Am.dot(x0)),
                                 tol=1e-08, maxiter=1e4)

print('difference between x from LU and CG solver...')
print('dif x =', np.linalg.norm(xV+x0-xlam[:xV.size]))
lam=invBB.solve(Bm.dot(f-Am.dot(x0)-Am.dot(xV)))
print('dif lambda =', np.linalg.norm(lam-xlam[xV.size:]))
print("""  The error about 1e-8 is influenced by the tolerance in conjugate gradient solver.
  Decreased tolerance would lead to decreased error.""")

print ('\n ****************** END ****************** ')
