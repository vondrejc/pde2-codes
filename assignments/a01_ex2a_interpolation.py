# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 15:05:26 2019

@author: mohit
"""

import numpy as np
import matplotlib.pyplot as pl
import scipy.interpolate as interpolate

f1 = lambda x: np.exp(x) # Function
f2 = lambda x: 1./(1+25*x**2) # Function
bound = [-1, 1] # Interval

# Function to get integration points and weights
def newton_cotes_rules(node, bound):
    pi = np.linspace(bound[0], bound[1], node)
    wi = np.zeros_like(pi)
    for ii in range(node):
        vals = np.zeros(node)
        vals[ii] = 1.
        lagpol = interpolate.lagrange(pi, vals)
        lagpol_int = lagpol.integ(1)
        wi[ii] = lagpol_int(bound[1])-lagpol_int(bound[0])
    return pi, wi

# N is Number of sample points
for N in [3,6,15]:
    for f in [f1,f2]:
        # x = np.linspace(bound[0], bound[1], N)
        # lg_poly = interpolate.lagrange(x, f(x))
        pi, wi=newton_cotes_rules(N, bound)
        leg_poly=interpolate.lagrange(pi, f(pi))
        piG, wiG=np.polynomial.legendre.leggauss(N)
        lag_poly=interpolate.lagrange(piG, f(piG))

        # Plot
        x=np.linspace(bound[0], bound[1], 1e3)
        pl.figure(figsize=(10, 8), dpi=60)
        pl.title('Interpolation using equidistant points and roots of Legendre polynomials', fontsize=14)
        pl.plot(x, f(x), label='f(x)', linewidth=2)
        pl.plot(pi, f(pi), 'rx', label='Lagrange points', linewidth=2)
        pl.plot(x, leg_poly(x), 'r-', label='Interpolation via Lagrange p.', linewidth=2)
        pl.plot(piG, f(piG), 'k+', label='Legendre points', linewidth=2)
        pl.plot(x, lag_poly(x), 'k--', label='Interpolation via Legendre p.', linewidth=2)
        # pl.plot(pi, wi, 'bo', label='Newton-Cotes Points/Weights')
        # pl.plot(piG, wiG, 'ko', label='Gauss-Legendre Points/Weights')
        pl.gca().set(xlim=(bound[0], bound[1]), xlabel='x-coordinates', ylabel='Function value')
        pl.legend(loc='best')
        pl.show()
print("""Hint: try to change N and the function f from f1 to f2 (line 13)
to see the Runge phenomenon.""")
