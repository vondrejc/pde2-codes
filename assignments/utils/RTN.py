import numpy as np
import scipy as sp
import sympy as smp
import matplotlib.pyplot as pl

def get_normals(coor):
    n_v, dim = coor.shape
    normals = np.zeros([n_v, dim])
    for ii in np.arange(n_v):
        vec_edge = coor[(ii + 1) % n_v] - coor[ii]
        normals[ii] = np.array([vec_edge[1], -vec_edge[0]])
        normals[ii] = normals[ii]/np.linalg.norm(normals[ii])
    return normals


def get_dofs_coor(coor, order=1):
    n_v = coor.shape[0]
    dofcoor = np.zeros(coor.shape)
    for ii in np.arange(n_v):
        dofcoor[ii] = 0.5 * (coor[(ii+1) % n_v] + coor[ii])
    return dofcoor

def get_edge_length(coor):
    n_v = coor.shape[0]
    e_size = np.zeros(n_v)
    for ii in np.arange(n_v):
        e_size[ii] = np.linalg.norm(coor[(ii+1)%n_v]-coor[ii])
    return e_size

def get_area(coor):
    n_v = coor.shape[0]
    mtx = np.ones((n_v, n_v), np.float64)
    mtx[0:n_v-1, :] = np.transpose(coor)
    return np.linalg.det(mtx)/2

def get_basis_fun(coor, diff=False):
    n_v = coor.shape[0]
    dim = coor.shape[1]
    # area, edge_length
    T_size = get_area(coor)
    e_size = get_edge_length(coor)
    # basis functions
    if diff is False:
        bas_fun = [lambda x: 1./(2*T_size)*(x-coor[0]),
                   lambda x: -1./(2*T_size)*(x-coor[1]),
                   lambda x: 1./(2*T_size)*(x-coor[2])]
    elif diff == 1 or diff == 'grad':
        raise
        bas_fun = lambda ii, _: np.eye(dim)*e_size[ii]/(2*T_size)
    elif diff == 'div':
        bas_fun = [lambda _: 1./T_size,
                   lambda _: -1./T_size,
                   lambda _: 1./T_size]
    else:
        NotImplementedError('This type of \
                            diff basis function is not implemented.')
    return bas_fun

class Mapping():
    def __init__(self, nodes):
        self.nodes = nodes
        self.b = nodes[0]
        self.A = np.vstack([nodes[1]-nodes[0], nodes[2]-nodes[0]]).T
        self.Ai = np.linalg.inv(self.A)

    def __call__(self, hx):
        return self.A.dot(hx)+self.b

    def jacobian(self):
        return self.A

    def det_jacobian(self):
        return np.linalg.det(self.A)

class Integration():
    def __init__(self, order):
        if order == 1:
            self.iw = np.array([1./2])
            self.ip = np.array([[1./3, 1./3]])
        elif order == 2:
            self.iw = 1./6*np.ones(3)
            self.ip = np.array([[1./6, 1./6],
                                [2./3, 1./6],
                                [1./6, 2./3]])
        else:
            raise NotImplementedError()

    def get_integration(self):
        return self.iw, self.ip

