import numpy as np
import subprocess
from dolfin import Mesh, MeshEditor


class JVMesh():

    def generate_geo(self):
        pass

    def generate_mesh(self):
        subprocess.call(['gmsh', self.filen+'.geo', '-2',
                         '-o', self.filen+'.msh', '-format', 'msh'])
        subprocess.call(['dolfin-convert', self.filen+'.msh', self.filen+'.xml'])
        return self.filen+'.msh'

    def generate(self):
        self.generate_geo()
        self.generate_mesh()


class Square(JVMesh):

    def __init__(self, filen='test', dim=2, elmsize=0.1, subdomain=None):
        self.filen = filen
        self.dim = dim
        self.elmsize = elmsize
        self.subdomain = subdomain

    def get_cellvertices(self, dim=2):
        if dim==2:
            cellvertices=np.array([[0.,0.],
                                   [1.,0.],
                                   [1.,1.],
                                   [0.,1.]])
        return cellvertices

    def write_point(self, gf, ii, coord, elmsize='elmsize'):
        if self.dim == 2:
            c = np.zeros(3)
            c[:2] = coord
        else:
            c = coord
        gf.write('Point(%d) = {%e, %e, %e, %s};\n' \
                 % (ii, c[0], c[1], c[2], str(elmsize)))

    def generate_geo(self):
        with open(self.filen + '.geo', 'w') as gf:
            gf.write('elmsize = %e;\n' % self.elmsize)
            npt = 0
            cellvertices = self.get_cellvertices(self.dim)
            # Vertices of the cell
            for ii, vert in enumerate(cellvertices):
                npt += 1
                self.write_point(gf, npt, vert)

            # Edges of the cell
            nedges = len(cellvertices)
            nln = 0
            for ii in range(nedges):
                nln += 1
                gf.write('Line(%d) = {%d, %d};\n' \
                         % (nln, ii+1, (ii+1) % nedges+1))
            gf.write(
                'Line Loop(0) = {'
                + ', '.join([str(pt) for pt in range(1, nedges+1)])
                +'};\n')

            ## subdomains
            if self.subdomain is None:
                gf.write('Plane Surface(0) = {0};\n')
            else:
                for point in self.subdomain:
                    npt += 1
                    self.write_point(gf, npt, point)

                bgl = 5
                bgp = 5
                nedges = self.subdomain.shape[0]
                for ii in range(nedges):
                    gf.write('Line(%d) = {%d, %d};\n' \
                             % (ii+bgl, ii+bgp, (ii + 1) % nedges + bgp))

                gf.write('Line Loop(1) = {'
                         + ', '.join([str(pt) for pt in range(bgl, bgl+nedges)])
                         + '};\n')
                gf.write('Plane Surface(0) = {0, 1};\n')
                gf.write('Plane Surface(1) = {1};\n')


class Polygon(Square):
    def __init__(self, filen='test', elmsize=0.1,
                 vertices=np.array([[0., 0.],
                                    [1., 0.],
                                    [1., 1.],
                                    [0., 1.]]),
                 vert_elmsize=None):
        self.filen = filen
        self.dim = vertices.shape[1]
        self.elmsize = elmsize
        self.vertices = vertices

    def generate_geo(self):

        with open(self.filen + '.geo', 'w') as gf:
            gf.write('elmsize = %e;\n' % self.elmsize)

            # Vertices of the cell
            npt = 0
            for ii, vert in enumerate(self.vertices):
                npt += 1
                self.write_point(gf, npt, vert, )

            # Edges of the cell
            nedges = self.vertices.shape[0]
            nln = 0
            for ii in range(nedges):
                nln += 1
                gf.write('Line(%d) = {%d, %d};\n' \
                         % (nln, ii+1, (ii+1) % nedges+1))
            gf.write(
                'Line Loop(0) = {'
                + ', '.join([str(pt) for pt in range(1, nedges+1)])
                +'};\n')

            gf.write('Plane Surface(0) = {0};\n')


def get_mesh_from_nodes_cells(nodes, cells):
    editor = MeshEditor()
    mesh = Mesh()
    editor.open(mesh,'triangle', 2, 2)
    editor.init_vertices(nodes.shape[0])
    editor.init_cells(cells.shape[0])
    [editor.add_vertex(i,n) for i,n in enumerate(nodes)]
    [editor.add_cell(i,n) for i,n in enumerate(cells)]
    editor.close()
    return mesh


import matplotlib as mpl
import matplotlib.pyplot as pl
import matplotlib.tri as tri


def plot_mesh(mesh, filen=None, note='_me', nodes=False, weak_bg=True,
                  xlbl='y'):

    mpl.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
    params = {'text.usetex' : True,
              'text.latex.unicode': True,
              'legend.fontsize': 10,
              }
    mpl.rcParams.update(params)
    parf = {'dpi' : 1000,
                'facecolor' : 'w',
                'edgecolor' : 'k',
                'figsize' : (4, 3),
                'figsize_square' : (3, 3),
                'pad_inches' : 0.05,
               }

    if filen is None:
        filen='test.eps'

    print ('plotting mesh...')

    def mesh2triang(mesh):
        xy = mesh.coordinates()
        return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

    pl.figure(num=None, figsize=parf['figsize'], dpi=parf['dpi'])
    pl.triplot(mesh2triang(mesh), color='b', linewidth=0.5)

    pl.xlabel(r'Coordinate $%s_1$' % xlbl)
    pl.ylabel(r'Coordinate $%s_2$' % xlbl)
    pl.axes().set_aspect('equal')
    pl.savefig(filen)
    pl.savefig(filen, dpi=parf['dpi'],
               pad_inches=parf['pad_inches'], bbox_inches='tight')
    pl.close()
    print('done')


if __name__=='__main__':
    exec(compile(open('../examples/FFTHvsFEM/solvers.py', "rb").read(), '../examples/FFTHvsFEM/solvers.py', 'exec'))