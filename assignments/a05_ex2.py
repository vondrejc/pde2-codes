"""
The primal formulation of Poisson's equation:

         -div(grad(u)) = f    in Omega
              du/dn = g    on Gamma_N
                  u = u_D  on Gamma_D

The corresponding weak (variational problem)

    <grad(u), grad(v)> = <f, v> + <g, v>  for all v

"""

# from __future__ import print_function
from dolfin import *
import numpy as np
import matplotlib.pylab as pl
import scipy.linalg as sclin

n_ele=10 # number of elements
p=1 # polynomial order

s=(3, 5)
J_primal=np.zeros(s)
J_dual=np.zeros(s)

iter_row=0

for p in [1, 2, 3]: # ]:
    iter_col=0
    for ele in [5, 10, 20]: # ,15,20,40]:

        mesh=UnitSquareMesh(ele, ele)
        # energies
        n=FacetNormal(mesh)
        Jprim=lambda u: assemble(0.5*inner(grad(u), grad(u))*dx-f*u*dx-sigN*u*ds(1))
        Jdual=lambda sigma: assemble(-0.5*inner(sigma, sigma)*dx+uD*dot(sigma, n)*ds(2))

        # Define source functions

        f=Expression("1000*(x[0] - 1)*x[0]*(x[1] - 1)*x[1]", degree=4) # source of heat
        uD=Expression("1", degree=0) # Dirichlet values
        sigN=Expression("10", degree=0) # Neumann values

        # Define Boundaries
        class Dirichlet_boundary(SubDomain):

            def inside(self, x, on_boundary):
                return on_boundary and (x[0]<DOLFIN_EPS or x[0]>1.0-DOLFIN_EPS)

        dirichlet_boundary=Dirichlet_boundary()

        class Neumann_boundary(SubDomain):

            def inside(self, x, on_boundary):
                return on_boundary and (x[1]<DOLFIN_EPS or x[1]>1.0-DOLFIN_EPS)

        neumann_boundary=Neumann_boundary()

        boundary_markers=MeshFunction('size_t', mesh, mesh.geometry().dim()-1)
        boundary_markers.set_all(0)
        neumann_boundary.mark(boundary_markers, 1)
        dirichlet_boundary.mark(boundary_markers, 2)
        ds=Measure('ds', domain=mesh, subdomain_data=boundary_markers)

        def solve_primal(p=1):
            # Define function spaces and mixed (product) space
            V=FunctionSpace(mesh, "CG", p)

            # Define trial and test functions
            u=TrialFunction(V)
            v=TestFunction(V)

            # Define variational form
            a=dot(grad(u), grad(v))*dx
            L=f*v*dx+sigN*v*ds(1)

            bc=DirichletBC(V, uD, dirichlet_boundary)
            # Compute solution
            u=Function(V)
            solve(a==L, u, bc)
            return u

        def solve_mixed_primal(mesh, p=1, method='primal_mixed'):
            Psig=VectorElement("DG", mesh.ufl_cell(), p-1)
            Pu=FiniteElement("CG", mesh.ufl_cell(), p)
            W=FunctionSpace(mesh, Psig*Pu)

            # Define trial and test functions
            (sigma, u)=TrialFunctions(W)
            (tau, v)=TestFunctions(W)
            # Define variational form
            a=(dot(sigma, tau)-dot(grad(u), tau)-dot(sigma, grad(v)))*dx
            L=-f*v*dx-sigN*v*ds(1)

            bc=DirichletBC(W.sub(1), uD, dirichlet_boundary)
            if method=='primal_mixed':
                # Compute solution for mixed primal
                w=Function(W)
                solve(a==L, w, bc)
                (sigma, u)=w.split(deepcopy=True)
                return (sigma, u)
            elif method=='fenics_linear_solver' or method=='explicit_linear_solver':
                VPsig=VectorFunctionSpace(mesh, 'DG', p-1)
                VPu=FunctionSpace(mesh, Pu)

                # assembling of a system matrix and RHS vector
                A=assemble(a)
                b=assemble(L)

                if method=='fenics_linear_solver':
                    # apply BCs
                    bc.apply(A, b)

                    # compute solution using Fenics
                    solution=sclin.solve(A.array(), b.get_local())
                    w=Function(W)
                    w.vector()[:]=solution
                    (sigma, u)=w.split(deepcopy=True)
                    return (sigma.vector().get_local(), u.vector().get_local(), w.vector().get_local())
                else:
                    # get indices of boundary
                    bcinds=list(bc.get_boundary_values().keys())

                    # get values on boundary
                    uD_sub=np.array(list(bc.get_boundary_values().values()))

                    # Gather active DOFs
                    adof=np.setdiff1d(np.arange(W.dim(), dtype=np.int32),
                                              bcinds).astype(np.int32)

                    # submatrix corresponding to active DOFs and to BC
                    A_PP=A.array()[adof, :][:, adof]
                    # submatrix corresponding to active DOFs and to BC
                    A_PD=A.array()[adof, :][:, bcinds]
                    # submatrix corresponding to BC
                    A_DD=A.array()[bcinds, :][:, bcinds]

                    # Compute solution for explicit assembling of coefficient matrix and RHS vector
                    u=np.zeros(W.dim()) # preallocationg
                    u[bcinds]=uD_sub # setting the values on Dirichlet boundary
                    u[adof]=np.linalg.solve(A_PP, b[adof]-A_PD.dot(uD_sub)) # solves the system for active DOFs

                    return (sigma, u)
            else:
                NotImplementedError('This method is not implemented.')

        def solve_mixed_dual(p=1, method='fenics_linear_solver'):
            Psig=FiniteElement("RT", mesh.ufl_cell(), p)
            Pu=FiniteElement("DG", mesh.ufl_cell(), p-1)
            W=FunctionSpace(mesh, Psig*Pu)

            # Define trial and test functions
            (sigma, u)=TrialFunctions(W)
            (tau, v)=TestFunctions(W)

            n=FacetNormal(mesh)
            a=(dot(sigma, tau)+div(tau)*u+div(sigma)*v)*dx
            L=-f*v*dx+uD*dot(tau, n)*ds(2)

            # Define function G such that G \cdot n = g
            class BoundarySource(UserExpression):

                def __init__(self, mesh, **kwargs):
                    self.mesh=mesh
                    super().__init__(**kwargs)

                def eval_cell(self, values, x, ufc_cell):
                    cell=Cell(self.mesh, ufc_cell.index)
                    n=cell.normal(ufc_cell.local_facet)
                    g=sigN(x)
                    values[0]=g*n[0]
                    values[1]=g*n[1]

                def value_shape(self):
                    return (2,)

            G=BoundarySource(mesh, degree=1)

            bc=DirichletBC(W.sub(0), G, neumann_boundary)

            if method=='fenics_linear_solver':
                # Compute solution using Fenics
                w=Function(W)
                solve(a==L, w, bc)
                (sigma, u)=w.split(deepcopy=True)
                return (sigma, u, w.vector().get_local())
            elif method=='explicit_linear_solver':
                # assembling of a system matrix and RHS vector
                A=EigenMatrix()
                A=assemble(a, tensor=A)
                b=assemble(L)
                # get indices of boundary
                bcinds=list(bc.get_boundary_values().keys())

                # get values on boundary
                uD_sub=np.array(list(bc.get_boundary_values().values()))

                # Gather active DOFs
                adof=np.setdiff1d(np.arange(W.dim(), dtype=np.int32),
                                          bcinds).astype(np.int32)

                # submatrix corresponding to active DOFs and to BC
                A_PP=A.array()[adof, :][:, adof]
                # submatrix corresponding to active DOFs and to BC
                A_PD=A.array()[adof, :][:, bcinds]
                # submatrix corresponding to BC
                A_DD=A.array()[bcinds, :][:, bcinds]

                # Compute solution for explicit assembling of system matrix and RHS vector
                u=np.zeros(W.dim()) # preallocationg
                u[bcinds]=uD_sub # setting the values on Dirichlet boundary
                u[adof]=np.linalg.solve(A_PP, b[adof]-A_PD.dot(uD_sub)) # solves the system for active DOFs
                return (sigma, u)

            else:
                NotImplementedError('This method is not implemented.')

        print('\n******** For ', p, ' order of polynomial and ', ele, ' no. of elements ********')

        uP=solve_primal(p=p)
        # solving primal mixed formulation
        # specify method as 'primal-mixed','fenics_linear_solver' or 'explicit_linear_solver'
        print('solving primal mixed formulation ...')
        (sigmaMP, uMP)=solve_mixed_primal(mesh, p=p, method='primal_mixed')
        (sig_primal_mixed_linear, u_primal_mixed_linear, u_primal_mixed_fenics)=solve_mixed_primal(mesh, p=p, method='fenics_linear_solver')
        (sig_primal_mixed_linear_ex, u_primal_mixed_linear_ex)=solve_mixed_primal(mesh, p=p, method='explicit_linear_solver')
        print('norm of dif between Fenics solution and explicitely derived solution =', np.linalg.norm(u_primal_mixed_linear_ex-u_primal_mixed_fenics))

        # solving dual mixed formulation
        # specify fenics_linear_solver' or 'explicit_linear_solver'
        print('solving dual mixed formulation ...')
        (sigmaMD, uMD, uMDfenics)=solve_mixed_dual(p=p, method='fenics_linear_solver')
        (sigmaMDex, uMDex)=solve_mixed_dual(p=p, method='explicit_linear_solver')
        print('norm of dif between Fenics solution and explicitely derived solution =', np.linalg.norm(uMDex-uMDfenics))

        print('norm of dif between primal and primal mixed linear form. = ', np.linalg.norm(uP.vector().get_local()-u_primal_mixed_linear))
        print('norm of dif between primal and primal mixed form. = ', np.linalg.norm(uP.vector().get_local()-uMP.vector().get_local()))
        print('primal energy = ', Jprim(uP))
        print('mixed primal energy = ', Jprim(uMP))
        print('dual mixed energy = ', Jdual(sigmaMD))

        """the dual energy is always smaller than the primal one;
        during the refinement of discretisation parameters (increasing N or p),
        the values converge to the same limit energy"""

        J_primal[iter_row][iter_col]=Jprim(uMP)
        J_dual[iter_row][iter_col]=Jdual(sigmaMD)

        iter_col=iter_col+1

    iter_row=iter_row+1

print('*********************** Task c ***********************')
fig, ax=pl.subplots()

ax.plot(np.array([5, 10, 15, 20, 40]), J_primal[0][:], label='p=1')
ax.plot(np.array([5, 10, 15, 20, 40]), J_primal[1][:], label='p=2')
ax.plot(np.array([5, 10, 15, 20, 40]), J_primal[2][:], label='p=3')

legend=ax.legend(loc='upper right', shadow=True, fontsize='x-large')
pl.title('primal-mixed energy functional')

fig, ax=pl.subplots()

ax.plot(np.array([5, 10, 15, 20, 40]), J_dual[0][:], label='p=1')
ax.plot(np.array([5, 10, 15, 20, 40]), J_dual[1][:], label='p=2')
ax.plot(np.array([5, 10, 15, 20, 40]), J_dual[2][:], label='p=3')

legend=ax.legend(loc='lower right', shadow=True, fontsize='x-large')
pl.title('dual-mixed energy functional')
pl.show()

print('END')
