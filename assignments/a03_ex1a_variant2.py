# Assignment 3

from dolfin import *
import numpy as np

# convergence_calculator() is a function defined to take in the order of polynomial as an input argument and prints out the convergence rate based on 2 different mesh size of 100*100 and 200*200.
def calculate_variational_problem(nx, p=1):
    # Create mesh and define function space.
    mesh=UnitSquareMesh(nx, nx)
    V=FunctionSpace(mesh, 'CG', p)

    # Define homogeneous dirichlet boundary condition
    u0=Constant(0.0)

    def u0_boundary(x, on_boundary):
        return on_boundary

    bc=DirichletBC(V, u0, u0_boundary)

    # Define variational problem
    u=TrialFunction(V)
    v=TestFunction(V)

    # Calculation of the right hand side f involves taking first the gradient of the analytical solution and then one should take the divergence of the resulting gradient. Remember, gradient of a scalar is a vector and diveregence of a gradient is a scalar. By doing the calculation by hand one should get the below mentioned function. One can notice that the degee here has been 2 less tahn in the analytical solutiuon owing to the fact that we have differentiated twice during the course of calculating f.
    f=Expression('10*sin(pi*x[1])*(2 + pi*pi*x[0]*(1 - x[0]))', degree=3)
    a=inner(nabla_grad(u), nabla_grad(v))*dx
    L=f*v*dx

    # Compute solution
    u=Function(V)
    solve(a==L, u, bc)
    return u

def calculate_convergence_rate(errors, nxs):
    rates = np.log(errors[:-1]/errors[1:])/np.log(nxs[1:]/nxs[:-1])
    return rates

nxfine=80
finemesh=UnitSquareMesh(nxfine,nxfine)
u_ref = calculate_variational_problem(nx=nxfine,p=4) # almost exact solution

for p in [1,2,3]: # loop over different polynomial order
    print("============================================")
    print("order of polynomial approximation = "+str(p))
    nxs=[10, 20, 40] # number of elements for calculation of conv. rates
    nxs=np.array(nxs)
    l2_error=np.zeros(nxs.size)
    h1_error=np.zeros(nxs.size)
    for ii, nx in enumerate(nxs):
        print('calculation on mesh with n={0} (or h={1})'.format(nx, 1./nx))
        u = calculate_variational_problem(nx, p=p)

        # Error calculation using the inbuilt errornorm() function of FeNICS
        l2_error[ii]=errornorm(u_ref, u, 'l2', 2, finemesh)
        h1_error[ii]=errornorm(u_ref, u, 'h1', 2, finemesh)

    print('Errors in L2-Norm = {}'.format(l2_error))
    print('Errors in H1-Norm = {}'.format(h1_error))

    print('Convergence rates in L2 Norm are {}'.format(calculate_convergence_rate(l2_error, nxs)))
    print('Convergence rates in H1 Norm are {}'.format(calculate_convergence_rate(h1_error, nxs)))

# To calculate the convergence rate, call the function with the suitable polynomial degree:

print('''\nOne can notice that, the convergence rate in the H1 norm is one less
than the converegence rate in the l2 norm.
Also the convergence rate increases with the increase
with the degree of the polynomial approximation.''')






