# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 22:37:55 2019d

@author: mohit
"""
from dolfin import *
import numpy as np
import matplotlib.pylab as pl

# convergence_calculator() is a function defined to take in the order of polynomial as an input argument
# and prints out the convergence rate based on 2 different mesh size of 100*100 and 200*200.
def calculate_variational_problem(nx, p):
    # Create mesh and define function space.
    mesh=UnitSquareMesh(nx, nx)
    V=FunctionSpace(mesh, 'CG', p)

    tol=1E-14

    # Subdomain for 1st Dirichlet boundary
    class Dirichlet_boundary_1(SubDomain):

        def inside(self, x, on_boundary):
            return on_boundary and (near(x[0], 0, tol) or near(x[0], 1, tol))

    # Subdomain for 2nd Dirichlet boundary
    class Dirichlet_boundary_2(SubDomain):

        def inside(self, x, on_boundary):
            return on_boundary and (near(x[1], 1, tol) and x[0]<1/2)

    # Subdomain for 1st Neumann boundary
    class Neumann_boundary_1(SubDomain):

        def inside(self, x, on_boundary):
            return on_boundary and (near(x[1], 1, tol) and x[0]>1/2)

    # Subdomain for 2nd Neumann boundary
    class Neumann_boundary_2(SubDomain):

        def inside(self, x, on_boundary):
            return on_boundary and (near(x[1], 0, tol))

    # Create mesh function over the cell
    boundary_markers=MeshFunction('size_t', mesh, mesh.geometry().dim()-1)

    # Mark all facets in the mesh as part of subdomain 0
    boundary_markers.set_all(0)

    # Mark 1st Dirichlet boundary condition as subdomain 1
    Dirichlet_boundary_1().mark(boundary_markers, 1)
    # Mark 2nd Dirichlet boundary condition as subdomain 2
    Dirichlet_boundary_2().mark(boundary_markers, 2)
    # Mark 1st Neumann boundary condition as subdomain 3
    Neumann_boundary_1().mark(boundary_markers, 3)
    # Mark 2nd Neumann boundary condition as subdomain 4
    Neumann_boundary_2().mark(boundary_markers, 4)

    # Define measure ds in terms of boundary markers to express integrals over the boundary parts
    ds=Measure('ds', domain=mesh, subdomain_data=boundary_markers)

    # Define variational problem
    u=TrialFunction(V)
    v=TestFunction(V)

    # Fluxes at Neumann boundaries
    fN1=Constant(0.0)
    fN2=Constant(-1.0)

    # Define homogeneous dirichlet boundary condition
    u0=Constant(0.0)
    bc=[DirichletBC(V, u0, boundary_markers, 1), DirichletBC(V, u0, boundary_markers, 2)]

    # Right hand side f = 0
    # f=Expression('10*sin(pi*x[1])*(2 + pi*pi*x[0]*(1 - x[0]))', degree=3)
    f=Constant(0.0)
    a=inner(nabla_grad(u), nabla_grad(v))*dx
    L=f*v*dx+fN1*v*ds(3)+fN2*v*ds(4) # integration over subdomain part 3 and 4

    # Compute solution
    u=Function(V)
    solve(a==L, u, bc)
    return u


def calculate_convergence_rate(errors, nxs):
    rates=np.log(errors[:-1]/errors[1:])/np.log(nxs[1:]/nxs[:-1])
    return rates


# Calculate reference solution with fine mesh and higher order ( almost exact solution )
nxfine=200
finemesh=UnitSquareMesh(nxfine, nxfine)
u_ref=calculate_variational_problem(nx=nxfine, p=3)

for p in [1, 2, 3]: # loop over different polynomial order
    print ("============================================")
    print ("order of polynomial approximation = "+str(p))
    nxs=[5, 10, 20, 40] # number of elements for calculation of conv. rates
    nxs=np.array(nxs)
    l2_error=np.zeros(nxs.size)
    h1_error=np.zeros(nxs.size)
    for ii, nx in enumerate(nxs):
        print ('calculation on mesh with n={0} (or h={1})'.format(nx, 1./nx))
        u=calculate_variational_problem(nx, p)

        # Error calculation using the inbuilt errornorm() function of FeNICS
        l2_error[ii]=errornorm(u_ref, u, 'l2', 2*p, finemesh)
        h1_error[ii]=errornorm(u_ref, u, 'h1', 2*p, finemesh)

    print ('Errors in L2-Norm = {}'.format(l2_error))
    print ('Errors in H1-Norm = {}'.format(h1_error))

    print ('Convergence rates in L2 Norm are {}'.format(calculate_convergence_rate(l2_error, nxs)))
    print ('Convergence rates in H1 Norm are {}'.format(calculate_convergence_rate(h1_error, nxs)))

    # Plotting for L2 error and H1 error
    # pl.figure()
    # pl.title('Errors', fontsize=14)
    # pl.loglog(nxs, l2_error, label='L2norm:pol_order={}'.format(p))
    # pl.loglog(nxs, h1_error, label='H1norm:pol_order={}'.format(p))
    # pl.gca().set(xlabel='Mesh size', ylabel='Error')
    # pl.legend(loc='best')
    # pl.show()

    # Plotting for L2 and H1 convergence rate
    pl.figure(p)
    pl.title('Convergence Rate', fontsize=14)
    pl.plot(nxs[1:], calculate_convergence_rate(l2_error, nxs), label='L2norm:pol_order={}'.format(p))
    pl.plot(nxs[1:], calculate_convergence_rate(h1_error, nxs), label='H1norm:pol_order={}'.format(p))
    pl.gca().set(xlabel='Mesh size', ylabel='Convergence rate')
    pl.legend(loc='best')
    pl.savefig('conv_rate_ex1b_p{}.eps'.format(p))

# To calculate the convergence rate, call the function with the suitable polynomial degree:

print ('''\nOne can notice that, the convergence rate in the H1 norm is one less
than the converegence rate in the l2 norm.
Also the convergence rate decreases with the increase
with the degree of the polynomial approximation.
The number of meshes can be also increased on line 42 by setting e.g.
nxs=[10, 20, 40, 80]
''')

# Plot of a solution and its derivative
import matplotlib.pyplot as pl
from mpl_toolkits.axes_grid1 import make_axes_locatable
ax=pl.subplot(121)
pl.title('Solution u', fontsize=14)
plot_u=plot(u)
pl.gca().set(xlabel='x1', ylabel='x2')
divider=make_axes_locatable(ax)
cax=divider.append_axes("right", size="5%", pad=0.05)
pl.colorbar(plot_u, cax)

ax=pl.subplot(122)
pl.title('Partial derivative of u wrt x (du/dx)', fontsize=14)
plot_dudx=plot(u, dx())
pl.gca().set(xlabel='x1', ylabel='x2')
pl.subplots_adjust(bottom=0.1, right=1.5, top=0.9)
divider=make_axes_locatable(ax)
cax=divider.append_axes("right", size="5%", pad=0.05)
pl.colorbar(plot_dudx, cax)

pl.show()

print('''One can observe the well distributed boundary values from the solution.
i.e, zero value for two Dirichlet boundaries (left and right faces) and one Neumann boundary (top face).
And nonzero value at one Neumann boundary (bottom face).''')
