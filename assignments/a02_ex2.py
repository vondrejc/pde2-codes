"""
Homework
Assignment 1
Exercise 2b, 2c and 2d
"""
from __future__ import print_function
from fenics import *
import numpy as np

# Define function
f=Expression('x[0]*x[0]*x[0]*x[1]', degree=4)

## (b) ###############################################
print('== Exercise 2b ==================================')
"""This is to show that the calculated norms approach the theoretical value
with increase in mesh resolution"""
for n in [10, 50, 100, 200, 500, 1000]:

    # Create mesh and define function space
    mesh=UnitSquareMesh(n, n) # Discretising the domain
    V=FunctionSpace(mesh, 'CG', 1) # Trial and Test spaces

    # Interpolating values of the function over the domain
    f_val=interpolate(f, V) # Interpolating values of the function over the mesh
    der_f_val=grad(f_val)

    # Evaluating norms
    L2_norm=sqrt(assemble((f_val)**2*dx))
    H1_semi_norm=sqrt(assemble(inner(der_f_val, der_f_val)*dx))
    H1_norm=sqrt(assemble((f_val)**2*dx+inner(der_f_val, der_f_val)*dx))

    print('L2 norm for n =', n, ' =', L2_norm)
    print('H1 seminorm for n =', n, ' =', H1_semi_norm)
    print('H1 norm for n =', n, ' =', H1_norm , '\n')


## (c) ################################################
print('== Exercise 2c ==================================')
for p in range(1, 6):

    # Create mesh and define function space
    mesh=UnitSquareMesh(5, 5) # Discretising the domain
    V=FunctionSpace(mesh, 'CG', p) # Trial and Test spaces

    # Interpolating values of the function over the domain
    f_val=interpolate(f, V) # Interpolating values of the function over the mesh
    der_f_val=grad(f_val)

    # Evaluating norms
    L2_norm=sqrt(assemble((f_val)**2*dx))
    H1_semi_norm=sqrt(assemble(inner(der_f_val, der_f_val)*dx))
    H1_norm=sqrt(assemble((f_val)**2*dx+inner(der_f_val, der_f_val)*dx))

    print('L2 norm for p =', p, ' =', L2_norm)
    print('H1 seminorm for p =', p, ' =', H1_semi_norm)
    print('H1 norm for p =', p, ' =', H1_norm , '\n')

# It can be seen that exact L2 and H1 norm and semi norm are achieved for p=4

# ## (d) ################################################
print('== Exercise 2d ==================================')
# # calculate norm of the following vector of components
mesh=UnitSquareMesh(1, 1) # Discretising the domain (only two elements are enough)
V=FunctionSpace(mesh, 'CG', 4) # Trial and Test spaces
u=TrialFunction(V)
v=TestFunction(V)

f_val=interpolate(f, V)
fvec=f_val.vector().get_local()
A=assemble(u*v*dx).array()
L2_norm_new=lambda vec:np.inner(np.dot(A, vec), vec)**0.5
print('Alternate definition of the L2 norm =', L2_norm_new(fvec))

B=assemble(dot(grad(u), grad(v))*dx).array()
H1_semi_norm_new=lambda vec:np.inner(np.dot(B, vec), vec)**0.5
print('Alternate definition of the H1 semi-norm =', H1_semi_norm_new(fvec))

H1_norm_new=(L2_norm_new(fvec)**2+H1_semi_norm_new(fvec)**2)**0.5
print('Alternate definition of the H1 norm =', H1_norm_new)

H1_norm_new2=lambda vec:np.inner(np.dot(A+B, vec), vec)**0.5
print('Alternate definition of the H1 norm =', H1_norm_new2(fvec))

print('END')
