print("""
Solution to Assingment 3, Exercise 2
====================================
""")

from dolfin import *
# from dolfin.cpp.mesh import UnitSquareMesh
import scipy.sparse.linalg as spslin
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np

meshsizes=5*2**np.arange(4)
print('meshsizes =',meshsizes)

dofs=np.zeros(len(meshsizes))
conditionnumber=np.zeros(len(meshsizes))
sparsity=np.zeros(len(meshsizes))

# define boundary condition
def u0_Boundary(x, on_boundary):
    return on_boundary


for ind, meshsize in enumerate(meshsizes):
    # create mesh
    print('---------------------')
    print('mesh size {0}*{0}'.format(meshsize))
    mesh=UnitSquareMesh(meshsize, meshsize)
    V=FunctionSpace(mesh, 'CG', 1)

    bc=DirichletBC(V, Constant(0.0), u0_Boundary)

    # define coeff. Matrix
    A=Expression('1+10*x[0]*(x[0]-1)*x[1]*(x[1]-1)', degree=2)
    # source term
    f=5

    # define variational problem
    u=TrialFunction(V)
    v=TestFunction(V)
    a=inner(A*grad(u), grad(v))*dx
    L=f*v*dx

    # compute solution
    u=Function(V)
    solve(a==L, u, bc)

    # assembling a system matrix
    K=EigenMatrix()
    K=assemble(a, tensor=K)

    # Get the indices corresponding to the boundary
    boundary_index=bc.get_boundary_values().keys()

    # dof contains a sorted array of indices which excludes constrained nodes
    wodof=np.setdiff1d(np.arange(V.dim()), boundary_index).astype(np.int32)

    Km=K.array()[wodof, :][:, wodof]
    Km_sparse=K.sparray()[wodof, :][:, wodof]

    dofs[ind]=V.dim() # number of DOFs (number of basis functions)
    conditionnumber[ind]=np.linalg.cond(Km[1:]) # calculate condition number
    print("Condition number = {}".format(conditionnumber[ind]))
    sparsity[ind]=float(Km_sparse.nnz) / np.prod(np.array(Km_sparse.shape))
    print('Sparsity = {}'.format(sparsity[ind]))

print('\nCOMMENTS TO THE RESULTS:\n')
print('''Exercise 2 (a):
The condition number of system matrix increases for increasing number of DOFs
(btw, this calls for using preconditioners).
There is an increase in the condition number in spite of the fact that ellipticity and continuity
constants of the bilinear form remains the same for all functions.
This is typical for FEM because the condition number is obtained
w.r.t. Euclidean norm of the coefficients and not to the Sobolev norm.
One could overcome that by using orthogonal basis functions with e.g. Sobolev scalar product.
Then the condition number would remain confined by ellipticity constant and continuity
constant of bilinear form.
Those methods with orthogonal basis functions are called spectral methods.

Compare this approach to the Conjugate gradients studied in PDE1.
When we use directly vectors from Krylov subspace, the condition number is also getting worse.
If we orthogonalise the basis functions, we get an improvement.
For A-orthonormal basis, we get identity matrix and thus condition number is 1. 
''')

print('''Exercise 2 (b):
Sparsity of the system matrix relates to the connectivity of the elements.
For bigger meshes, the connectivity is lower and there is lower number of non-zero values
in system matrix.
''')

print('''Exercise 2 (c):
The LU-decomposition or Cholesky decomposition decreases the sparsity of the factors compared to
the original system. There it requires more memory to store the factors than original system.
Different methods provide different patterns depending on the permutation of the degrees of freedom.
''')

plt.figure()
plt.plot(dofs, conditionnumber, 'ko-')
plt.title('Condition number for increasing number of DOFS')
plt.xlabel("Number of degrees of freedom (dofs)")
plt.ylabel("Condition number")

plt.figure()
plt.plot(dofs, sparsity, 'ko-')
plt.title('Sparsity of system matrix for increasing DOFS (method=COLAMD)')
plt.xlabel("Number of degrees of freedom (dofs)")
plt.ylabel("Sparsity (no. of non-zero elements / no. of all elements)")

plt.figure()
plt.title('Nonzero values in the system matrix; sparsity={0}'.format(sparsity[-1]))
plt.spy(Km_sparse, marker='o')


for method in ['NATURAL', 'MMD_ATA', 'MMD_AT_PLUS_A', 'COLAMD']:
    lu = spslin.splu(Km_sparse.tocsc(), permc_spec=method)
    plt.figure()
    plt.title('LU-decomposition (method={}): L matrix.'.format(method))
    plt.spy(lu.L, marker='o')

    plt.figure()
    plt.title('LU-decomposition (method={}): U matrix.'.format(method))
    plt.spy(lu.U, marker='o')

plt.show()
